
# Leodoc's API Symfony Client Bundle

Leodoc provides an API to use it outside of the website. The doc is available on a page : https://app.leodoc.net/api/doc

This Symfony Client Bundle uses the API endpoints to permit faster integrations in your solutions.

## Requirements

PHP 7.4 and later
Symfony 5.3 and later

## Getting Started

Update .env file
```
# .env
LEODOC_SYMFONY_CLIENT_BEARER=your_jwt_token
LEODOC_SYMFONY_CLIENT_TEST=true
```

Create config/packages/leodoc_symfony_client.yaml
```
# leodoc_symfony_client.yaml
leodoc_symfony_client:  
  bearer : '%env(LEODOC_SYMFONY_CLIENT_BEARER)%'
  test : '%env(bool:LEODOC_SYMFONY_CLIENT_TEST)%'
```

## Installation & Usage
### Composer

Install with [Composer](http://getcomposer.org/):

```
composer require leodoc/symfony-client-bundle
```

Further do:
* run ``composer install`` to get dependencies added to your vendor directory

### Usage
Two examples using the bundle from a Symfony Controller :
```php
<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Psr\Log\LoggerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Leodoc\SymfonyClientBundle\Service\{Fusion, Template};

class TestController extends AbstractController
{
    public function __construct(LoggerInterface $logger, Template $template, Fusion $fusion)
    {
        $this->logger = $logger;
        $this->template = $template;
        $this->fusion = $fusion;
    }

    /**
     * Retrieve account's templates
     * @Route("/template", methods={"GET"})
     */
    public function getTemplates()
    {
        $templates = $this->template->getAll();

        $response = new Response();
        $response->setContent(json_encode($templates, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Send an asynchronous request to create several PDFs using a CSV
     * @Route("/fusion/csv", methods={"GET"})
     */
    public function postFusionMassCsv()
    {
        $params["csv"] = "your_base64_csv";
        $params["template_id"] = "your_template_id";

        $notifications["email"] = ["subject" => "test api leodoc", "addresses" => ["mail@test.com"]];
        $notifications["webcallback"] = ["urls" => ["https://webhook.site/some-id"]];

        $fusion = $this->fusion->postMassCsv($params, $notifications);

        $response = new Response();
        $response->setContent(json_encode($fusion, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }

}

?>
```

## Documentation for using bundle services

Don't hesitate to check this bundle's services in order to read annotations for each method.

Service | Method | Description |
------- | ------ | ----------- |
*Template* | getAll | Retrieve account's templates informations
*Download* | getZip | Retrieve fusion's zip
*Fusion* | postOne | Send a synchronous request to create one PDF
*Fusion* | postEditable | Create an editable fusion
*Fusion* | postMassCsv | Send an asynchronous request to create several PDFs using a CSV
*Fusion* | postMassJson | Send an asynchronous request to create several PDFs using JSON
*Fusion* | get | Retrieve informations about a fusion
*Fusion* | getAll | Retrieve account's fusions informations
*Fusion* | delete | Delete a fusion
*Fusion* | getDownload | Retrieve fusion's download URLs

If you need help, or if you have some feedback, don't hesitate to contact us at support[a]leodoc.net