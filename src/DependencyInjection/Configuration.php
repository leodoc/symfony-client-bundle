<?php
namespace Leodoc\SymfonyClientBundle\DependencyInjection;

use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

class Configuration implements ConfigurationInterface
{


    function getConfigTreeBuilder()
    {
        $treeBuilder  = new TreeBuilder('leodoc_symfony_client');

        $treeBuilder->getRootNode()
            ->children()
            ->scalarNode('bearer')->isRequired()->cannotBeEmpty()->end()
            ->variableNode('url')->defaultValue('https://app.leodoc.net/api')->end()
            ->booleanNode('test')->defaultValue(false)->end();

        return $treeBuilder;
    }
}