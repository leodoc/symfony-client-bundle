<?php
namespace Leodoc\SymfonyClientBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Leodoc\SymfonyClientBundle\DependencyInjection\LeodocSymfonyClientExtension;

/**
 * LeodocSymfonyClientBundle
 */
class LeodocSymfonyClientBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $ext = new LeodocSymfonyClientExtension([],$container);
    }
}