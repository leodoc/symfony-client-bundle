<?php

namespace Leodoc\SymfonyClientBundle\Service;

use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Template
{

    public function __construct(HttpClientInterface $client, ParameterBagInterface $params)
    {
        $this->client = $client;
        $this->params = $params;
    }

    /**
     * Retrieve account's templates informations
     *
     * @param string $token Fusion's id
     * 
     * @return Templates' informations.
     */ 
    public function getAll()
    {
        $response = $this->client->request(
            'GET',
            $this->params->get('leodoc_symfony_client.url').'/templates',
            [
                'headers' => [
                'Content-Type' => 'application/json',
                ]
            ]
        );
        
        return $response->toArray();
    }

}
