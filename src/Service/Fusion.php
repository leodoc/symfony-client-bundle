<?php

namespace Leodoc\SymfonyClientBundle\Service;

use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Fusion
{

    public function __construct(HttpClientInterface $client, ParameterBagInterface $params)
    {
        $this->client = $client;
        $this->params = $params;
    }

    /**
     * Send a synchronous request to create one PDF
     *
     * @param array $params Contains template_id or twig (twig's base64), data
     * 
     * @return PDF's base64
     */ 
    public function postOne(array $params)
    {
        $response = $this->client->request(
            'POST',
            $this->params->get('leodoc_symfony_client.url').'/fusion',
            [
                'body' => $params,
                'headers' => [
                    'Content-Type: application/json',
                ],
            ]
        );
        
        return $response->toArray()["pdf"];
    }

    /**
     * Create an editable fusion
     *
     * @param array $params Contains template_id, data, webcallback_url
     * 
     * @return A link to edit a fusion
     */ 
    public function postEditable(array $params)
    {
        $response = $this->client->request(
            'POST',
            $this->params->get('leodoc_symfony_client.url').'/editable-fusion',
            [
                'body' => $params,
                'headers' => [
                    'Content-Type: application/json',
                ],
            ]
        );
        
        return $response->toArray()["url"];
    }

    /**
     * Send an asynchronous request to create several PDFs using a CSV
     *
     * @param array $params Contains template_id or twig (twig's base64), csv (csv's base64)
     * @param array $notifications Contains email (array with subject and adresses which is an array of mail adresses) and/or webcallback (array with urls which is an array of URLs)
     * 
     * @return Created fusion informations
     */ 
    public function postMassCsv(array $params, array $notifications)
    {
        $response = $this->client->request(
            'POST',
            $this->params->get('leodoc_symfony_client.url').'/fusion/async/csv',
            [
                'body' => array_merge($params, ["notification" => $notifications]),
                'headers' => [
                    'Content-Type: application/json',
                ],
            ]
        );
        
        return $response->toArray();
    }

    /**
     * Send an asynchronous request to create several PDFs using JSON
     *
     * @param array $params Contains template_id or twig (twig's base64), json (json's base64)
     * @param array $notifications Contains email (array with subject and adresses which is an array of mail adresses) and/or webcallback (array with urls which is an array of URLs)
     * 
     * @return Created fusion informations
     */ 
    public function postMassJson(array $params, array $notifications)
    {
        $response = $this->client->request(
            'POST',
            $this->params->get('leodoc_symfony_client.url').'/fusion/async/json',
            [
                'body' => array_merge($params, ["notification" => $notifications]),
                'headers' => [
                    'Content-Type: application/json',
                ],
            ]
        );
        
        return $response->toArray();
    }

    /**
     * Retrieve informations about one fusion
     *
     * @param string $token Fusion's id
     * 
     * @return Fusion's informations
     */ 
    public function get(string $token)
    {
        $response = $this->client->request(
            'GET',
            $this->params->get('leodoc_symfony_client.url').'/fusion/'.$token,
            [
                'headers' => [
                'Content-Type' => 'application/json',
                ]
            ]
        );
        
        return $response->toArray();
    }

    /**
     * Retrieve account's fusions informations
     *
     * @param string $token Fusion's id
     * 
     * @return Fusions' informations
     */ 
    public function getAll()
    {
        $response = $this->client->request(
            'GET',
            $this->params->get('leodoc_symfony_client.url').'/fusion',
            [
                'headers' => [
                'Content-Type' => 'application/json',
                ]
            ]
        );
        
        return $response->toArray();
    }

    /**
     * Delete a fusion
     *
     * @param string $token Fusion's id
     * 
     * @return Fusion's informations
     */ 
    public function delete(string $token)
    {
        $response = $this->client->request(
            'DELETE',
            $this->params->get('leodoc_symfony_client.url').'/fusion/'.$token,
            [
                'headers' => [
                'Content-Type' => 'application/json',
                ]
            ]
        );
        
        return $response->toArray();
    }

    /**
     *  Retrieve fusion's download URLs
     *
     * @param string $token Fusion's id
     * 
     * @return Fusion's URLs
     */ 
    public function getDownload(string $token)
    {
        $response = $this->client->request(
            'GET',
            $this->params->get('leodoc_symfony_client.url').'/fusion/download/urls/'.$token,
            [
                'headers' => [
                'Content-Type' => 'application/json',
                ]
            ]
        );
        
        return $response->toArray();
    }
}
