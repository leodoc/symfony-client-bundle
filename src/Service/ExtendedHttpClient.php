<?php

namespace Leodoc\SymfonyClientBundle\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Symfony\Contracts\HttpClient\ResponseStreamInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use JMS\Serializer\SerializerInterface;

class ExtendedHttpClient implements HttpClientInterface
{
    private $decoratedClient;

    public function __construct(HttpClientInterface $decoratedClient = null, ParameterBagInterface $params, SerializerInterface $serializer)
    {
        $this->decoratedClient = $decoratedClient ?? HttpClient::create();
        $this->params = $params;
        $this->serializer = $serializer;
    }

    public function request(string $method, string $url, array $options = []): ResponseInterface
    {
        if(str_contains(debug_backtrace()[1]["class"], "Leodoc\SymfonyClientBundle"))
        {
            // process and/or change the $method, $url and/or $options as needed
            $options["headers"]["Authorization"] = 'Bearer '.$this->params->get('leodoc_symfony_client.bearer');
            $options["body"]["test"] = $this->params->get('leodoc_symfony_client.test');
            $options["body"] = $this->serializer->serialize($options["body"], 'json');

            //If not on the main app we do not verify the host in order to dev
            if (!str_contains($url, 'app.leodoc.net'))
            {
                $options['verify_peer'] = false;
            }
        }

        // if you call here any method on $response, the HTTP request
        // won't be async; see below for a better way
        $response = $this->decoratedClient->request($method, $url, $options);
        return $response;
    }

    public function stream($responses, float $timeout = null): ResponseStreamInterface
    {
        return $this->decoratedClient->stream($responses, $timeout);
    }

    public function withOptions(array $options): static
    {
        $clone = clone $this;
        $clone->client = $this->client->withOptions($options);

        return $clone;
    }
}