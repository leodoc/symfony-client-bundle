<?php

namespace Leodoc\SymfonyClientBundle\Service;

use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class Download
{

    public function __construct(HttpClientInterface $client, ParameterBagInterface $params)
    {
        $this->client = $client;
        $this->params = $params;
    }

    /**
     *  Retrieve fusion's zip
     *
     * @param string $token Fusion's id
     * 
     * @return Fusion's URLs
     */ 
    public function getZip(string $token)
    {
        $response = $this->client->request(
            'GET',
            $this->params->get('leodoc_symfony_client.url').'/fusion/download/zip/'.$token,
            [
                'headers' => [
                'Content-Type' => 'application/json',
                ]
            ]
        );
        
        return $response->getContent();
    }
}
